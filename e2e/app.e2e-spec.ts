import { GfDaDepressaoPage } from './app.po';

describe('gf-da-depressao App', () => {
  let page: GfDaDepressaoPage;

  beforeEach(() => {
    page = new GfDaDepressaoPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});

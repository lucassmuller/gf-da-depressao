import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraucComponent } from './grauc.component';

describe('GraucComponent', () => {
  let component: GraucComponent;
  let fixture: ComponentFixture<GraucComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraucComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraucComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-grauc',
  templateUrl: './grauc.component.html',
  styleUrls: ['./grauc.component.css']
})
export class GraucComponent implements OnInit {
  calculado: boolean = false;
  aprovado: boolean = false;
  ga: number;
  gb: number;
  gf: number;
  gaResultado: number;
  gbResultado: number;

  constructor() { }

  ngOnInit() {
  }

  calcula() {
    this.calculado = false;
    this.aprovado = false;
    if(this.ga >= 0 && this.ga <=10 && this.gb >= 0 && this.gb <=10) {
      this.gf = this.notaGF();
      if(this.gf >= 6) {
        this.aprovado = true;
      } else {
        this.gaResultado = this.calculoNotaGA();
        this.gbResultado = this.calculoNotaGB();
        this.calculado = true;
      }
    }
  }

  notaGF(): number {
    let resultado: number;
    resultado = ((this.ga * 0.33) + (this.gb * 0.67));
    return resultado;
  }

  calculoNotaGB(): number {
    let result: number;
    result = (6 - (this.ga * 0.33))/0.67;
    return result;
  }

  calculoNotaGA(): number {
    let result: number;
    result = (6 - (this.gb * 0.67))/0.33;
    return result;
  }
}
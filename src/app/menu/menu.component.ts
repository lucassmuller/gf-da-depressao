import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  opcoes: Array<{
    titulo: String,
    texto: String,
    btTexto: String,
    btLink: String
  }>;

  constructor() { 
    this.opcoes = [
      {
        titulo: "Grau Final - Calcular nota",
        texto: `Acabou o semestre e quer calcular sua nota? final? Use essa ferramenta e calcule.`,
        btTexto: "Calcular nota",
        btLink: "/ferramentas/grauf"
      },
      {
        titulo: "Grau C - Qual nota substituir?",
        texto: `Você ficou todas as aulas olhando vídeos de gatos brincando, 
      stalkiando o seu crush ou Netflix e quando viu chegou as provas se fu*eu? 
      Clique nessa opção e veja qual nota substituir.`,
        btTexto: "Ver minha melhor chance",
        btLink: "/ferramentas/grauc"
      }
    ];
  }

  ngOnInit() {
  }

}

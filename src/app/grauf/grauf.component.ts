import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-grauf',
  templateUrl: './grauf.component.html',
  styleUrls: ['./grauf.component.css']
})
export class GraufComponent implements OnInit {
  calculado: boolean = false;
  aprovado: boolean = false;
  ga: number;
  gb: number;
  gf: number;

  constructor() { }

  ngOnInit() {
  }

  calcula() {
    this.calculado = false;
    this.aprovado = false;
    let resultado: number;

    if(this.ga >= 0 && this.ga <=10 && this.gb >= 0 && this.gb <=10) {
      resultado = ((this.ga * 0.33) + (this.gb * 0.67));
      this.gf = resultado;
      if(resultado >= 6)
        this.aprovado = true;
      else
        this.aprovado = false;
      this.calculado = true;
    }
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraufComponent } from './grauf.component';

describe('GraufComponent', () => {
  let component: GraufComponent;
  let fixture: ComponentFixture<GraufComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraufComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraufComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

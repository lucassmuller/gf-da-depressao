import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  appName = "Grau Final da Depressão";

  constructor(private title: Title){
    this.title.setTitle(this.appName);
  }
}
